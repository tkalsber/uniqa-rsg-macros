package at.uits.rest;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class RsgResourcesModel {

    @XmlElement(name = "value")
    private String message;

    public RsgResourcesModel() {
    }

    public RsgResourcesModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}