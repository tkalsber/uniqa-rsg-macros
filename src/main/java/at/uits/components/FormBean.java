package at.uits.components;

import at.uits.servlet.stream.FormItems;
import com.google.common.collect.Lists;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by thomas on 23.10.2015.
 */
public class FormBean {

    // logging feature
    private static final Logger LOG = LoggerFactory.getLogger(FormBean.class);

    // enumeration for fieldkeys
    public enum FieldKey  {
        NEUBESICHTIGUNG("neubesichtigung"),
        NACHBESICHTIGUNG("nachbesichtigung"),
        NACHBESICHTIGUNG_SCHADEN("nachbesichtigung-schaden"),
        BETRIEB_UND_PLANEN("betrieb-und-planen"),
        BP_WERT_GEBAEUDE("bp-wert-gebaeude"),
        BP_WERT_EINRICHTUNG("bp-wert-einrichtung"),
        BP_RISIKO_KOMPLETT("bp-risiko-komplett"),
        BP_PML("bp-pml"),
        BP_FEUER("bp-feuer"),
        BP_RISIKO_ED("bp-risiko-ed"),
        BP_RISIKO_NATKAT("bp-risiko-natkat"),
        BP_BESCHREIBUNG("bp-beschreibung"),

        HOF_UND_ERNTE("hof-und-ernte"),
        HE_GEBAEUDE("he-gebaeude"),
        HE_RISIKO("he-risiko"),
        HE_FLAECHE("he-flaeche"),
        HE_MASCHINE("he-maschine"),

        INDUSTRIE_UND_INDIVIDUAL("industrie-und-individual"),
        II_GEBAEUDE("ii-gebaeude"),
        II_EINRICHTUNG("ii-einrichtung"),
        II_RISIKO("ii-risiko"),

        IMMOBILIE_UND_VERWALTUNG("immobilie-und-verwaltung"),
        IV_GEBAEUDE("iv-gebaeude"),
        IV_RISIKO("iv-risiko"),

        SONSTIGES("sonstiges"),
        SONSTIGES_BESCHREIBUNG("sonstiges-beschreibung"),

        WEG_F980("weg-f980"),
        WEG_F639("weg-f639"),
        WEG_F401("weg-f401"),
        VOLLMACHT_BEI("vollmacht-bei"),

        KOSTENUEBERNAHME_JA("kostenuebernahme-ja"),
        KOSTENUEBERNAHME_NEIN("kostenuebernahme-nein"),
        PLANUNGSUNTERLAGEN_JA("planungsunterlagen-ja"),
        PLANUNGSUNTERLAGEN_NEIN("planungsunterlagen-nein"),
        EINRICHTUNG_BESCHREIBUNG("einrichtung-beschreibung"),

        AUFTRAGGEBER_NAME("auftraggeber-name"),
        AUFTRAGGEBER_TELEFON("auftraggeber-telefon"),
        AUFTRAGGEBER_EMAIL("auftraggeber-email"),
        AUFTRAGGEBER_JA("auftraggeber-ja"),
        AUFTRAGGEBER_NEIN("auftraggeber-nein"),

        VERTRIEBSPARTNER("vertriebspartner"),
        ANSPRECHPARTNER("ansprechpartner"),
        BETREUUNG_TELEFONNUMMER("betreuung-telefonnummer"),
        BETREUUNG_EMAIL("betreuung-email"),
        BETREUUNG_JA("betreuung-ja"),
        BETREUUNG_NEIN("betreuung-nein"),

        VERSICHERUNGSNEHMER("versicherungsnehmer"),
        VERSICHERUNGSNEHMER_POLIZZE("versicherungsnehmer-polizze"),
        VERSICHERUNGSNEHMER_TELEFON("versicherungsnehmer-telefon"),
        VERSICHERUNGSNEHMER_EMAIL("versicherungsnehmer-email"),

        ADRESSEN("adressen"),
        ZUSATZINFO("zusatzinfo"),

        PAGEID("pageid"),
        TARGETEMAIL("targetemail"),
        USEREMAIL("useremail"),
        TARGETPAGE("targetpage");

        private final String key;
        FieldKey(final String key) { this.key = key; }
        String key() { return this.key; }
    }


    // LEFT COLUMN
    public boolean neubesichtigung;
    public boolean nachbesichtigung;
    public boolean nachbesichtigung_schaden;

    public boolean betrieb_und_planen;
    public boolean bp_wert_gebaeude;
    public boolean bp_wert_einrichtung;
    public boolean bp_risiko_komplett;
    public boolean bp_pml;
    public boolean bp_feuer;
    public boolean bp_risiko_ed;
    public boolean bp_risiko_natkat;
    public String bp_beschreibung;

    public boolean hof_und_ernten;
    public boolean he_gebaeude;
    public boolean he_risiko;
    public boolean he_flaeche;
    public boolean he_maschine;

    public boolean industrie_und_individual;
    public boolean ii_gebaeude;
    public boolean ii_einrichtung;
    public boolean ii_risiko;

    public boolean immobilie_und_verwaltung;
    public boolean iv_gebaeude;
    public boolean iv_risiko;

    public String sonstiges_beschreibung;

    public boolean weg_f980;
    public boolean weg_f639;
    public boolean weg_f401;
    public String vollmacht_bei;
    public boolean kostenuebernahme;
    public boolean planungsunterlagen;

    public String einrichtung_beschreibung;

    // RIGHT COLUMN

    public String auftraggeber_name;
    public String auftraggeber_telefon;
    public String auftraggeber_email;
    public boolean auftraggeber_anwesenheit;


    public String vertriebspartner;
    public String ansprechpartner;
    public String betreuung_telefonnummer;
    public String betreuung_email;
    public boolean betreuung_anwesenheit;


    public String versicherungsnehmer;
    public String versicherungsnehmer_polizze;
    public String versicherungsnehmer_telefon;
    public String versicherungsnehmer_email;

    public List<String> risikoAnschriften = Lists.newArrayList();

    public String zusatzinfo;

    public String useremail;
    public String targetEmail;
    public String targetPageId;

    /**
     * Create new <code>FormBean</code> instance.
     *
     * @param formItems formitems
     */
    public FormBean(FormItems formItems) {
        if(formItems == null) {
            LOG.error("invalid argument:" + formItems);
            throw new IllegalArgumentException("formItems parameter can not be null.");
        }
        try {
            initFromFormItems(formItems);
        } catch (UnsupportedEncodingException e) {
            LOG.error(e.getMessage());
        }

    }

    public String getRisikoAnschriften() {
        StringBuilder sb = new StringBuilder();
        for(String a : this.risikoAnschriften)
            sb.append(a + "\n");
        return sb.toString();
    }

    public String getTargetPageId() {
        return targetPageId;
    }

    /*
     * Initialize the bean with FormItems.
     */
    private void initFromFormItems(FormItems formFields) throws UnsupportedEncodingException {
        final String DELIMITER = "###";
        // get addresses separated by '###'
        FileItem adressenField = formFields.fields.get(FieldKey.ADRESSEN.key());
        if(adressenField != null) {
            String separatedAdresses = URLDecoder.decode(adressenField.getString("UTF-8"), "UTF-8");
            String[] addresses = separatedAdresses.split(DELIMITER);
            for(String address : addresses) {
                if(StringUtils.isNotBlank(address) && !address.equals(DELIMITER)) {
                    this.risikoAnschriften.add(address);
                }
            }
        }

        // top-down, starting with left column
        this.neubesichtigung = getBooleanFieldValue(formFields, FieldKey.NEUBESICHTIGUNG.key());
        this.nachbesichtigung = getBooleanFieldValue(formFields, FieldKey.NACHBESICHTIGUNG.key());
        this.nachbesichtigung_schaden = getBooleanFieldValue(formFields, FieldKey.NACHBESICHTIGUNG_SCHADEN.key());

        this.betrieb_und_planen = getBooleanFieldValue(formFields, FieldKey.BETRIEB_UND_PLANEN.key());
        this.bp_wert_gebaeude = getBooleanFieldValue(formFields, FieldKey.BP_WERT_GEBAEUDE.key());
        this.bp_wert_einrichtung = getBooleanFieldValue(formFields, FieldKey.BP_WERT_EINRICHTUNG.key());
        this.bp_risiko_komplett = getBooleanFieldValue(formFields, FieldKey.BP_RISIKO_KOMPLETT.key());
        this.bp_pml = getBooleanFieldValue(formFields, FieldKey.BP_PML.key());
        this.bp_feuer = getBooleanFieldValue(formFields, FieldKey.BP_FEUER.key());
        this.bp_risiko_ed = getBooleanFieldValue(formFields, FieldKey.BP_RISIKO_ED.key());
        this.bp_risiko_natkat = getBooleanFieldValue(formFields, FieldKey.BP_RISIKO_NATKAT.key());
        this.bp_beschreibung = getStringFieldValue(formFields, FieldKey.BP_BESCHREIBUNG.key(), "");

        this.hof_und_ernten = getBooleanFieldValue(formFields, FieldKey.HOF_UND_ERNTE.key());
        this.he_gebaeude = getBooleanFieldValue(formFields, FieldKey.HE_GEBAEUDE.key());
        this.he_risiko = getBooleanFieldValue(formFields, FieldKey.HE_RISIKO.key());
        this.he_flaeche = getBooleanFieldValue(formFields, FieldKey.HE_FLAECHE.key());
        this.he_maschine = getBooleanFieldValue(formFields, FieldKey.HE_MASCHINE.key());

        this.industrie_und_individual = getBooleanFieldValue(formFields, FieldKey.INDUSTRIE_UND_INDIVIDUAL.key());
        this.ii_gebaeude = getBooleanFieldValue(formFields, FieldKey.II_GEBAEUDE.key());
        this.ii_einrichtung = getBooleanFieldValue(formFields, FieldKey.II_EINRICHTUNG.key());
        this.ii_risiko = getBooleanFieldValue(formFields, FieldKey.II_RISIKO.key());

        this.immobilie_und_verwaltung = getBooleanFieldValue(formFields, FieldKey.IMMOBILIE_UND_VERWALTUNG.key());
        this.iv_gebaeude = getBooleanFieldValue(formFields, FieldKey.IV_GEBAEUDE.key());
        this.iv_risiko = getBooleanFieldValue(formFields, FieldKey.IV_RISIKO.key());

        this.sonstiges_beschreibung = getStringFieldValue(formFields, FieldKey.SONSTIGES_BESCHREIBUNG.key(), "");

        this.weg_f980 = getBooleanFieldValue(formFields, FieldKey.WEG_F980.key());
        this.weg_f639 = getBooleanFieldValue(formFields, FieldKey.WEG_F639.key());
        this.weg_f401 = getBooleanFieldValue(formFields, FieldKey.WEG_F401.key());
        this.vollmacht_bei = getStringFieldValue(formFields, FieldKey.VOLLMACHT_BEI.key(), "");
        this.kostenuebernahme = getBooleanFieldValue(formFields, FieldKey.KOSTENUEBERNAHME_JA.key());
        this.planungsunterlagen = getBooleanFieldValue(formFields, FieldKey.PLANUNGSUNTERLAGEN_JA.key());

        this.einrichtung_beschreibung = getStringFieldValue(formFields, FieldKey.EINRICHTUNG_BESCHREIBUNG.key(), "");

        // right-column
        this.auftraggeber_name = getStringFieldValue(formFields, FieldKey.AUFTRAGGEBER_NAME.key(), "");
        this.auftraggeber_telefon = getStringFieldValue(formFields, FieldKey.AUFTRAGGEBER_TELEFON.key(), "");
        this.auftraggeber_email = getStringFieldValue(formFields, FieldKey.AUFTRAGGEBER_EMAIL.key(), "");
        this.auftraggeber_anwesenheit = getBooleanFieldValue(formFields, FieldKey.AUFTRAGGEBER_JA.key());

        this.vertriebspartner = getStringFieldValue(formFields, FieldKey.VERTRIEBSPARTNER.key(), "");
        this.ansprechpartner = getStringFieldValue(formFields, FieldKey.ANSPRECHPARTNER.key(), "");
        this.betreuung_telefonnummer = getStringFieldValue(formFields, FieldKey.BETREUUNG_TELEFONNUMMER.key(), "");
        this.betreuung_email = getStringFieldValue(formFields, FieldKey.BETREUUNG_EMAIL.key(), "");
        this.betreuung_anwesenheit = getBooleanFieldValue(formFields, FieldKey.BETREUUNG_JA.key());

        this.versicherungsnehmer = getStringFieldValue(formFields, FieldKey.VERSICHERUNGSNEHMER.key(), "");
        this.versicherungsnehmer_polizze = getStringFieldValue(formFields, FieldKey.VERSICHERUNGSNEHMER_POLIZZE.key(), "");
        this.versicherungsnehmer_telefon = getStringFieldValue(formFields, FieldKey.VERSICHERUNGSNEHMER_TELEFON.key(), "");
        this.versicherungsnehmer_email = getStringFieldValue(formFields, FieldKey.VERSICHERUNGSNEHMER_EMAIL.key(), "");

        this.zusatzinfo = getStringFieldValue(formFields, FieldKey.ZUSATZINFO.key(), "");

        this.useremail = getStringFieldValue(formFields, FieldKey.USEREMAIL.key(), "");
        this.targetEmail = getStringFieldValue(formFields, FieldKey.TARGETEMAIL.key(), "");
        this.targetPageId = getStringFieldValue(formFields, FieldKey.TARGETPAGE.key(), "");
    }

    private String getStringFieldValue(FormItems formFields, String fieldName, String defaultValue)  {
        if(formFields == null || StringUtils.isBlank(fieldName)) {
            LOG.error("Invalid arguments");
            return "";
        }
        // check if checkbox/radiobutton exists.
        // return true on success, else false
        FileItem field = formFields.fields.get(fieldName);
        if(field == null)
            return defaultValue;
        else {
            String encodedString = null;
            try {
                encodedString = field.getString("utf-8");
                return encodedString;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "utf8-encoding failed";
            }
        }
    }

    private boolean getBooleanFieldValue(FormItems formFields, String fieldName) {
        if(formFields == null || StringUtils.isBlank(fieldName)) {
            LOG.error("Invalid arguments");
            return false;
        }
        // check if checkbox/radiobutton exists.
        // return true on success, else false
        FileItem field = formFields.fields.get(fieldName);
        if(field == null) return false;
        else return true;
    }

    private boolean getBooleanFieldValue(FormItems formFields, String fieldName, boolean defaultValue) {
        if(formFields == null || StringUtils.isBlank(fieldName)) {
            LOG.error("Invalid arguments");
            return false;
        }
        // check if checkbox/radiobutton exists.
        // return true on success, else false
        FileItem field = formFields.fields.get(fieldName);
        if(field == null) return defaultValue;
        else return true;
    }


    public String getTargetEmail() {
        return targetEmail;
    }

    public String getUserEmail() { return useremail; }

    public String getAuftraggeberEmail() { return auftraggeber_email; }

    @Override
    public String toString() {
        StringBuilder content = new StringBuilder();
        content.append("<b><u>Besichtigungsbeauftragung:</u></b><br>");
        if(neubesichtigung)             content.append("<br>Neubesichtigung: " + Boolean.TRUE);
        if(nachbesichtigung)            content.append("<br>Nachbesichtigung: " + Boolean.TRUE);
        if(nachbesichtigung_schaden)    content.append("<br>\tBesichtigung aufgrund Schadenfall: " + Boolean.TRUE);

        if(betrieb_und_planen)  content.append("<br>Betrieb und Planen: " + Boolean.TRUE);
        if(bp_wert_gebaeude)    content.append("<br>\tWertermittlung Geb&auml;ude: " + Boolean.TRUE);
        if(bp_wert_einrichtung) content.append("<br>\tWertermittlung Einrichtung: " + Boolean.TRUE);
        if(bp_risiko_komplett)  content.append("<br>\tRisikoevaluierung komplett: " + Boolean.TRUE);

        if(bp_pml)              content.append("<br>\t\tZus&auml;tzlich PML: " + Boolean.TRUE);
        if(bp_feuer)            content.append("<br>\tRisiko Feuer: " + Boolean.TRUE);
        if(bp_risiko_ed)        content.append("<br>\tRisikoevaluierung ED: " + Boolean.TRUE);
        if(bp_risiko_natkat)    content.append("<br>\tRisikoevaluierung NATKAT: " + Boolean.TRUE);
        if(StringUtils.isNotBlank(bp_beschreibung))  content.append("<br>\tBeschreibung:<br>\t" + bp_beschreibung);

        if(hof_und_ernten)      content.append("<br><br>Hof und Ernten: " + Boolean.TRUE);
        if(he_gebaeude)         content.append("<br>\tWertermittlung Geb&auml;ude: " + Boolean.TRUE);
        if(he_risiko)           content.append("<br>\tRisikoevaluierung (Landwirtschaft): " + Boolean.TRUE);
        if(he_flaeche)          content.append("<br>\t\tEvaluierung Fl&auml;che: " + Boolean.TRUE);
        if(he_maschine)         content.append("<br>\t\tEvaluierung Maschine: " + Boolean.TRUE);

        if(industrie_und_individual)    content.append("<br><br>Industrie und Individual: " + Boolean.TRUE);
        if(ii_gebaeude)                 content.append("<br>\tWertermittlung Geb&auml;ude: " + Boolean.TRUE);
        if(ii_einrichtung)              content.append("<br>\tWertermittlung Einrichtung: " + Boolean.TRUE);
        if(ii_risiko)                   content.append("<br>\tRisikoevaluierung: " + Boolean.TRUE);

        if(immobilie_und_verwaltung)    content.append("<br><br>Immobilie und Verwaltung: " + Boolean.TRUE);
        if(iv_gebaeude)                 content.append("<br>\tWertermittlung Geb&auml;ude: " + Boolean.TRUE);
        if(iv_risiko)                   content.append("<br>\tRisikoevaluierung: " + Boolean.TRUE);

        if(StringUtils.isNotBlank(sonstiges_beschreibung))      content.append("<br><br>\tSonstiges Beschreibung:<br>\t" + sonstiges_beschreibung);

        content.append("<br><br>Sch&auml;tzen laut Gruppierungserl&auml;uterung:");
        if(weg_f980)                                content.append("<br>Wertermittlung Geb&auml;ude: " + Boolean.TRUE);
        if(weg_f639)                                content.append("<br>Wertermittlung Einrichtung: " + Boolean.TRUE);
        if(weg_f401)                                content.append("<br>Risikoevaluierung: " + Boolean.TRUE);
        if(StringUtils.isNotBlank(vollmacht_bei))   content.append("<br>\tVollmacht Planeinsicht einzuholen bei:<br>\t" + vollmacht_bei);
        if(kostenuebernahme)                        content.append("<br>\tKosten&uuml;bernahmeverpflichtung: " + Boolean.TRUE);
        if(planungsunterlagen)                      content.append("<br>\tPlanungsunterlagen vorhanden: " + Boolean.TRUE);

        if(StringUtils.isNotBlank(einrichtung_beschreibung)) content.append("<br><br>\tWertermittlung Einrichtung Beschreibung:<br>\t" + einrichtung_beschreibung);

        content.append("<br><br>Auftraggeber");
        if(StringUtils.isNotBlank(auftraggeber_name))       content.append("<br>Auftraggeber Name: " + auftraggeber_name);
        if(StringUtils.isNotBlank(auftraggeber_telefon))    content.append("<br>Auftraggeber Telefon: " + auftraggeber_telefon);
        if(StringUtils.isNotBlank(auftraggeber_email))      content.append("<br>Auftraggeber Email: " + auftraggeber_email);
        if(auftraggeber_anwesenheit)                        content.append("<br>Gew&uumlnscht wird die Anwesenheit des Auftragsgebers: " + Boolean.TRUE);

        content.append("<br><br>Betreuung");
        if(StringUtils.isNotBlank(vertriebspartner))        content.append("<br>Vertriebspartner: " + vertriebspartner);
        if(StringUtils.isNotBlank(ansprechpartner))         content.append("<br>Name des Ansprechpartners: " + ansprechpartner);
        if(StringUtils.isNotBlank(betreuung_telefonnummer)) content.append("<br>Telefonnummer: " + betreuung_telefonnummer);
        if(StringUtils.isNotBlank(betreuung_email))         content.append("<br>Email Adresse: " + betreuung_email);
        if(betreuung_anwesenheit)                        content.append("<br>Gew&uumlnscht wird die Anwesenheit des Betreuers: " + Boolean.TRUE);

        content.append("<br><br>Versicherungsnehmer");
        if(StringUtils.isNotBlank(versicherungsnehmer))             content.append("<br>Versicherungsnehmer Name: " + versicherungsnehmer);
        if(StringUtils.isNotBlank(versicherungsnehmer_polizze))     content.append("<br>Polizze: " + versicherungsnehmer_polizze);
        if(StringUtils.isNotBlank(versicherungsnehmer_telefon))     content.append("<br>Telefonnummer: " + versicherungsnehmer_telefon);
        if(StringUtils.isNotBlank(versicherungsnehmer_email))       content.append("<br>Email Adresse: " + versicherungsnehmer_email);

        content.append("<br><br>RisikoAnschriften<br>");
        content.append(getRisikoAnschriften());

        content.append("<br><br>Zusatzinfo<br>");
        content.append(zusatzinfo);
        content.append("<br><br>-------------------------------------------------------------------------------------------------------<br>");

        return content.toString();
    }
}
