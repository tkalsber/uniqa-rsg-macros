package at.uits.components;

import java.io.Writer;
import java.util.Map;

/**
 * Created by thomas on 23.10.2015.
 */
public interface EmailRenderer {

    /**
     * Renders the formbean as HTML template.
     *
     * @param bean the bean to render
     * @param templateName the name of the templatefile
     * @return the rendered template
     */
    String renderEmail(final FormBean bean, final String templateName);

}
