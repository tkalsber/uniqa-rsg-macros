package at.uits.components.impl;

import at.uits.components.EmailRenderer;
import at.uits.components.FormBean;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by thomas on 23.10.2015.
 */
public class HtmlEmailRenderer implements EmailRenderer {

    private final TemplateRenderer templateRenderer;
    private static final Logger LOG = LoggerFactory.getLogger(HtmlEmailRenderer.class);

    private Map<String, Object> map = Maps.newHashMap();

    public HtmlEmailRenderer(TemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }

    private void addBoolean(boolean val, String key) {
        if(val) map.put(key, Boolean.TRUE);
        else map.put(key, Boolean.FALSE);
    }

    private Map<String, Object> toMap(FormBean bean) {
        this.map.clear();

        addBoolean(bean.neubesichtigung, "neubesichtigung");
        addBoolean(bean.nachbesichtigung, "nachbesichtigung");
        addBoolean(bean.nachbesichtigung_schaden, "nachbesichtigung_schaden");

        addBoolean(bean.betrieb_und_planen, "betrieb_und_planen");
        addBoolean(bean.bp_wert_gebaeude, "bp_wert_gebaeude");
        addBoolean(bean.bp_wert_einrichtung, "bp_wert_einrichtung");
        addBoolean(bean.bp_risiko_komplett, "bp_risiko_komplett");
        addBoolean(bean.bp_pml, "bp_pml");
        addBoolean(bean.bp_feuer, "bp_feuer");
        addBoolean(bean.bp_risiko_ed, "bp_risiko_ed");
        addBoolean(bean.bp_risiko_natkat, "bp_risiko_natkat");
        map.put("bp_beschreibung", bean.bp_beschreibung);

        addBoolean(bean.hof_und_ernten, "hof_und_ernten");
        addBoolean(bean.he_gebaeude, "he_gebaeude");
        addBoolean(bean.he_risiko, "he_risiko");
        addBoolean(bean.he_flaeche, "he_flaeche");
        addBoolean(bean.he_maschine, "he_maschine");

        addBoolean(bean.industrie_und_individual, "industrie_und_individual");
        addBoolean(bean.ii_gebaeude, "ii_gebaeude");
        addBoolean(bean.ii_einrichtung, "ii_einrichtung");
        addBoolean(bean.ii_risiko, "ii_risiko");

        addBoolean(bean.immobilie_und_verwaltung, "immobilie_und_verwaltung");
        addBoolean(bean.iv_gebaeude, "iv_gebaeude");
        addBoolean(bean.iv_risiko, "iv_risiko");

        map.put("sonstiges_beschreibung", bean.sonstiges_beschreibung);

        addBoolean(bean.weg_f980, "weg_f980");
        addBoolean(bean.weg_f639, "weg_f639");
        addBoolean(bean.weg_f401, "weg_f401");
        map.put("vollmacht_bei", bean.vollmacht_bei);
        addBoolean(bean.kostenuebernahme, "kostenuebernahme");
        addBoolean(bean.planungsunterlagen, "planungsunterlagen");
        map.put("einrichtung_beschreibung", bean.einrichtung_beschreibung);

        // RIGHT COLUMN
        map.put("auftraggeber_name", bean.auftraggeber_name);
        map.put("auftraggeber_telefon", bean.auftraggeber_telefon);
        map.put("auftraggeber_email", bean.auftraggeber_email);
        addBoolean(bean.auftraggeber_anwesenheit, "auftraggeber_anwesenheit");

        map.put("vertriebspartner", bean.vertriebspartner);
        map.put("ansprechpartner", bean.ansprechpartner);
        map.put("betreuung_telefonnummer", bean.betreuung_telefonnummer);
        map.put("betreuung_email", bean.betreuung_email);
        addBoolean(bean.betreuung_anwesenheit, "betreuung_anwesenheit");

        map.put("versicherungsnehmer", bean.versicherungsnehmer);
        map.put("versicherungsnehmer_polizze", bean.versicherungsnehmer_polizze);
        map.put("versicherungsnehmer_telefon", bean.versicherungsnehmer_telefon);
        map.put("versicherungsnehmer_email", bean.versicherungsnehmer_email);

        map.put("risikoAnschriften", bean.risikoAnschriften);

        if(bean.risikoAnschriften.size() > 0)
            map.put("risikoAnschriftenAdded", Boolean.TRUE);
        else
            map.put("risikoAnschriftenAdded", Boolean.FALSE);
        map.put("zusatzinfo", bean.zusatzinfo);
        map.put("targetEmail", bean.targetEmail);
        map.put("targetPageId", bean.targetPageId);

        return map;
    }

    @Override
    public String renderEmail(final FormBean bean, final String templateName) {
        try {
            StringWriter stringWriter = new StringWriter();
            this.templateRenderer.render(templateName, this.toMap(bean), stringWriter);
            return stringWriter.toString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return e.getMessage();
        }
    }

    private Multipart getMultipart() {
        /*
        if(this.attachedImages != null && !this.attachedImages.isEmpty()) {
            MimeMultipart multipart = new MimeMultipart("related");
            Iterator var2 = this.attachedImages.iterator();

            while(var2.hasNext()) {
                DataSource dataSource = (DataSource)var2.next();

                try {
                    String e = dataSource.getName();
                    multipart.addBodyPart(this.createMimeBodyPart(e, new DataHandler(dataSource), e));
                } catch (MessagingException var5) {
                    log.error("Could not create multipart attachment for email: " + this.subject, var5);
                }
            }

            return multipart;
        } else {
            return null;
        }
        */
        return null;
    }

    private MimeBodyPart createMimeBodyPart(String contentId, DataHandler dataHandler, String fileName) throws MessagingException {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setDataHandler(dataHandler);
        mimeBodyPart.setHeader("Content-ID", "<" + contentId + ">");
        if(StringUtils.isNotBlank(fileName)) {
            mimeBodyPart.setFileName(fileName);
        }

        return mimeBodyPart;
    }
}
