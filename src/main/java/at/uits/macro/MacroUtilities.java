/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.uits.macro;

import java.util.*;
import org.apache.commons.lang.*;
import com.atlassian.confluence.macro.*;
import com.atlassian.confluence.renderer.radeox.macros.*;
import com.atlassian.confluence.user.*;
import com.atlassian.confluence.languages.*;
import com.atlassian.confluence.util.*;
import com.atlassian.confluence.core.*;
import com.atlassian.renderer.*;
import com.atlassian.confluence.renderer.*;
import java.util.regex.*;
import com.atlassian.confluence.content.render.xhtml.*;

public class MacroUtilities
{
    private static final Pattern PARAGRAPH_PATTERN;
    private static final String TITLE_KEY = "title";
    private static final String TITLE_I18N = "i18n";
    
    public static Boolean getBooleanParameter(final Map<String, String> parameters, final String name, final boolean defaultValue) {
        final String parameterValue = parameters.get(name);
        if (parameterValue == null) {
            return defaultValue;
        }
        return Boolean.valueOf(parameterValue);
    }
    
    public static String getStringParameter(final Map<String, String> parameters, final String name, final String defaultValue) {
        String parameterValue = parameters.get(name);
        if (parameterValue == null) {
            parameterValue = parameters.get(name.toLowerCase());
        }
        if (parameterValue == null) {
            return defaultValue;
        }
        return parameterValue;
    }
    
    public static Integer getIntegerParameter(final Map<String, String> parameters, final String key, final Integer defaultValue) {
        String value = null;
        if (StringUtils.isNumeric((String)parameters.get(key))) {
            value = parameters.get(key);
        }
        else if (StringUtils.isNumeric((String)parameters.get(key.toLowerCase()))) {
            value = parameters.get(key.toLowerCase());
        }
        if (value == null) {
            return defaultValue;
        }
        Integer returnVal = null;
        try {
            returnVal = new Integer(parameters.get(key));
        }
        catch (Exception ex) {}
        if (returnVal == null) {
            return defaultValue;
        }
        return returnVal;
    }
    
    public static Map<String, Object> defaultVelocityContext(final MacroExecutionContext ctx) {
        final ConfluenceActionSupport action = (ConfluenceActionSupport)MacroUtils.getConfluenceActionSupport();
        final Map<String, Object> context = (Map<String, Object>)MacroUtils.defaultVelocityContext();
        context.put("page", ctx.getContent());
        if (ctx.getContent() != null) {
            context.put("pageId", ctx.getContent().getId());
        }
        context.put("remoteUser", AuthenticatedUserThreadLocal.getUser());
        context.put("spaceKey", ctx.getPageContext().getSpaceKey());
        context.put("action", action);
        context.put("helper", action.getHelper());
        return context;
    }
    
    public static Map<String, Object> defaultVelocityContextWithDateFormatter(final MacroExecutionContext ctx, final FormatSettingsManager formatSettingsManager, final LocaleManager localeManager) {
        final Map<String, Object> context = defaultVelocityContext(ctx);
        context.put("dateFormatter", new DateFormatter(GeneralUtil.getGlobalSettings().getTimeZone(), formatSettingsManager, localeManager));
        return context;
    }
    
    public static String createUniqueID(final Class className, final MacroExecutionContext ctx) {
        return createUniqueID(className, (RenderContext)ctx.getPageContext(), ctx.getParams());
    }
    
    public static String createUniqueID(final Class className, final RenderContext context, final Map<String, String> macroParams) {
        String defaultID = className.getSimpleName() + "_";
        if (context != null && context instanceof PageContext) {
            final PageContext ctx = (PageContext)context;
            if (ctx.getEntity() != null) {
                defaultID = defaultID + ctx.getEntity().getIdAsString() + "_";
            }
        }
        defaultID += Math.abs(macroParams.hashCode() + className.hashCode());
        return defaultID;
    }
    
    public static String stripFirstParagraphTags(final String xml) {
        final String trimmed = StringUtils.trim(xml);
        final Matcher matcher = MacroUtilities.PARAGRAPH_PATTERN.matcher(trimmed);
        if (matcher.find() && matcher.end() == trimmed.length()) {
            return matcher.group(1);
        }
        return xml;
    }
    
    public static String getTitleFromMacroParams(final Map<String, String> params) {
        String title = null;
        final String i18n = getStringParameter(params, "i18n", "");
        if (StringUtils.isNotBlank(i18n)) {
            title = ConfluenceActionSupport.getTextStatic(i18n);
        }
        if (StringUtils.isBlank(title)) {
            title = getStringParameter(params, "title", "");
        }
        return title;
    }
    
    public static Boolean isMobileView(final ConversionContext context) {
        return "mobile".equals(context.getPropertyAsString("output-device-type"));
    }

    public static ConfluenceUser getLoggedInUser() {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        return user;
    }

    static {
        PARAGRAPH_PATTERN = Pattern.compile("<p>(.*?)</p>", 34);
    }
    
    
}
