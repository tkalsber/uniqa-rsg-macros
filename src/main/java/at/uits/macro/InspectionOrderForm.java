package at.uits.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.xwork.SimpleXsrfTokenGenerator;
import com.atlassian.xwork.XsrfTokenGenerator;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Created by VZM9621 on 22.10.2015.
 */
public class InspectionOrderForm implements Macro
{

    private static final String	MACRO_TEMPLATE	= "templates/rsg-order-form.vm";

    private final XsrfTokenGenerator tokenGenerator					= new SimpleXsrfTokenGenerator();

    public InspectionOrderForm() {

    }

    @Override
    public String execute(Map<String, String> parameters, String body,
                          ConversionContext context) throws MacroExecutionException
    {
        Map<String, Object> velocityContext = MacroUtilities.defaultVelocityContext(new MacroExecutionContext(parameters, body, context.getPageContext()));
        String targetEmailAddress = parameters.get("targetaddress");
        String targetPage = parameters.get("targetpage");
        if(StringUtils.isBlank(targetEmailAddress))
            return "<h3>You MUST specify a target emailaddress first.</h3>";
        if(StringUtils.isBlank(targetPage))
            return "<h3>You MUST specify a redirect page first.</h3>";
        velocityContext.put("targetEmailAddress", targetEmailAddress);
        velocityContext.put("targetPage", targetPage);

        ConfluenceUser user = MacroUtilities.getLoggedInUser();
        if(user != null) {
            velocityContext.put("useremail", user.getEmail());
            velocityContext.put("username", user.getFullName());
        }
        return VelocityUtils.getRenderedTemplate(MACRO_TEMPLATE, velocityContext);
    }


    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}