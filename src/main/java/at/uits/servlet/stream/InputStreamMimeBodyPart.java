package at.uits.servlet.stream;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by vzm9621 on 29.10.2015.
 */
public class InputStreamMimeBodyPart extends MimeBodyPart {

    private InputStream inputStream;

    public InputStreamMimeBodyPart(InputStream source) {
        this.inputStream = source;
        if(!inputStream.markSupported()) {
            throw new IllegalArgumentException("only streams with mark supported are ok");
        }
        inputStream.mark(Integer.MAX_VALUE); // remeber the whole stream.
    }

    @Override
    protected InputStream getContentStream() throws MessagingException {
        throw new IllegalStateException("getContentStream is not implemented on purpose.");
    }

    @Override
    public void writeTo(OutputStream os) throws IOException, MessagingException {
        byte[] buf = new byte[32];
        int length;
        inputStream.reset();
        while((length = inputStream.read(buf)) > -1 ) {
            os.write(buf, 0, length);
        }
    }

}
