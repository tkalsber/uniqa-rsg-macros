package at.uits.servlet.stream;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by vzm9621 on 29.10.2015.
 */
public class FormItems {

    public static final int			THRESHOLD_SIZE					= 1024 * 1024 * 10;

    public Map<String, FileItem> fields = Maps.newHashMap();
    public List<FileItem> attachments = Lists.newArrayList();
    public File fileRepository;

    public static FormItems getFormItems(HttpServletRequest request, ServletContext servletContext) throws FileUploadException {
        FormItems items = new FormItems();

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(THRESHOLD_SIZE);

        items.fileRepository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(items.fileRepository);
        ServletFileUpload upload = new ServletFileUpload(factory);

        List tempItems = upload.parseRequest(request);
        Iterator iterator = tempItems.iterator();
        while (iterator.hasNext()) {
            FileItem item = (FileItem) iterator.next();
            if (item.isFormField())
                items.fields.put(item.getFieldName(), item);
            else {
                if(item.getSize() > 0)
                    items.attachments.add(item);
            }
        }

        return items;
    }
}
