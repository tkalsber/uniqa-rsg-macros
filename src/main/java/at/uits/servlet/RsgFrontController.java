package at.uits.servlet;

import at.uits.components.EmailRenderer;
import at.uits.components.FormBean;
import at.uits.servlet.stream.FormItems;
import com.atlassian.confluence.mail.ConfluencePopMailServer;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.mail.template.MultipartBuilder;
import com.atlassian.confluence.pages.*;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.Email;
import com.atlassian.mail.MailException;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.Set;

public class RsgFrontController extends HttpServlet
{

    private static final Logger LOG = LoggerFactory.getLogger(RsgFrontController.class);

    private final SettingsManager settingsManager;
    private final MailServerManager mailServerManager;
    private final FileUploadManager fileUploadManager;
    private final MultiQueueTaskManager taskManager;
    private final EmailRenderer emailRenderer;

    // documents can not be bigger than 10 MB
    private static final int			THRESHOLD_SIZE					= 1024 * 1024 * 10;
    private static final String         MAILQUEUE_NAME                  = "mail";
    private static final String         MAIL_TEMPLATE                   = "templates/email-template.vm";

    public RsgFrontController(SettingsManager settingsManager, MailServerManager mailServerManager, MultiQueueTaskManager taskManager, EmailRenderer emailRenderer) {
        super();
        this.emailRenderer = emailRenderer;
        Preconditions.checkNotNull(settingsManager, "Injection failed, SettingsManager is null.");
        this.settingsManager = settingsManager;

        Preconditions.checkNotNull(mailServerManager, "Injection failed, MailServerManager is null.");
        this.mailServerManager = mailServerManager;

        this.fileUploadManager = (FileUploadManager) ContainerManager.getComponent("fileUploadManager");
        Preconditions.checkNotNull(this.fileUploadManager, "Injection failed, FileUploadManager is null.");

        this.taskManager = taskManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
        response.setContentType("text/html");
        response.getWriter().write("Not Allowed");
        response.getWriter().flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        // handle multipart requests
        if (isMultipart) {

            final String baseUrl = this.settingsManager.getGlobalSettings().getBaseUrl();

            try {
                // parse all formitems
                final FormItems formItems = FormItems.getFormItems(request, this.getServletConfig().getServletContext());
                Set<String> keys = formItems.fields.keySet();
                File directory = new File(System.getProperty("java.io.tmpdir"));
                List<File> files = Lists.newArrayList();

                // init formbean with formitems
                FormBean bean = new FormBean(formItems);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.setPrettyPrinting().create();
                String json = gson.toJson(bean, FormBean.class);
                LOG.debug("FormBean Values:");
                LOG.debug(json);

                // attach form-attachments to page
                for(FileItem item : formItems.attachments) {

                    File attachmentFile = new File(directory + File.separator + item.getName());
                    files.add(attachmentFile);

                    OutputStream out = new FileOutputStream(attachmentFile);
                    InputStream filecontent = item.getInputStream();

                    int read = 0;
                    final byte[] bytes = new byte[1024];
                    while ((read = filecontent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    filecontent.close();
                    out.flush();
                    out.close();
                }
                com.atlassian.confluence.admin.actions.mail.SendTestEmailAction sendTestEmailAction;

                List<DataSource> dataSources = Lists.newArrayList();
                for(File attachment : files) {
                    DataSource dataSource = new FileDataSource(attachment);
                    dataSources.add(dataSource);
                }

                UAgentInfo uAgentInfo = new UAgentInfo(request);
                //boolean isIE = uAgentInfo.detectMSIE();

                String rsgEmailAddress = bean.getTargetEmail();
                String userEmailAddress = bean.getUserEmail();
                String requestorEmailAddress = bean.getAuftraggeberEmail();

                String mimeType = ConfluenceMailQueueItem.MIME_TYPE_HTML;
                String renderedEmail = emailRenderer.renderEmail(bean, MAIL_TEMPLATE);

                if(this.getSmtpMailServer() != null) {

                    ConfluenceMailQueueItem mailQueueItem = new ConfluenceMailQueueItem(rsgEmailAddress, "", getSubject(bean, false), renderedEmail, mimeType, dataSources);
                    taskManager.addTask(MAILQUEUE_NAME, mailQueueItem);

                    mailQueueItem = new ConfluenceMailQueueItem(requestorEmailAddress, "", getSubject(bean, true), renderedEmail, mimeType, dataSources);
                    taskManager.addTask(MAILQUEUE_NAME, mailQueueItem);

                    if(!requestorEmailAddress.equalsIgnoreCase(userEmailAddress)) {
                        mailQueueItem = new ConfluenceMailQueueItem(userEmailAddress, "", getSubject(bean, true), renderedEmail, mimeType, dataSources);
                        taskManager.addTask(MAILQUEUE_NAME, mailQueueItem);
                    }

                    taskManager.flush(MAILQUEUE_NAME);
                    LOG.debug("Email sent, redirecting to RSG form.");
                }
                else LOG.error("Could not send email, no SMTP mailserver found");

                // redirect to request source
                response.sendRedirect(this.settingsManager.getGlobalSettings().getBaseUrl() + "/pages/viewpage.action?pageId=" + bean.getTargetPageId());
            } catch (Exception e) {
                LOG.error("Sending of email failed:" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void sendEmail(String emailAddress, String subject, String body, String mimeType, List<DataSource> dataSources) {
        // get mailserver - if configured
        SMTPMailServer mailserver = this.getSmtpMailServer();
        if(mailserver == null) {
            LOG.error("Can not send rsg email, no mailserver configured.");
            return;
        }
        // create email
        Email email = new Email(emailAddress);
        email.setSubject(subject);
        email.setBody(body);
        email.setMimeType("text/html");
        email.setFrom("noreply@ma-portal.uniqa.at");
        email.setFromName("RSG MA-Portal");
        if(dataSources != null && dataSources.size() < 0)
            email.setMultipart(MultipartBuilder.INSTANCE.makeMultipart(dataSources));
        // send it
        try {
            mailserver.send(email);
        } catch (MailException e) {
            LOG.error("RSG email sending failed: " + e);
        }
    }

    private String getSubject(FormBean bean, boolean isAuftragsbestaetigung) {

        String subject = (isAuftragsbestaetigung) ?
                "Auftragsbestaetigung Besichtigung_Risiko"
                : "Besichtigung_Risiko";

        // add subsection(s)
        if(bean.betrieb_und_planen)
            subject += "_BUP";
        if(bean.hof_und_ernten)
            subject += "_HUE";
        if(bean.industrie_und_individual)
            subject += "_IUI";
        if(bean.immobilie_und_verwaltung)
            subject += "_IUV";
        if(StringUtils.isNotBlank(bean.sonstiges_beschreibung))
            subject += "_SON";

        // add neu- or nachbesichtigung
        if(bean.neubesichtigung) subject += " (Neu) ";
        else subject += " (Folge) ";

        subject += "AG: " + bean.auftraggeber_name;
        subject += " VN: " + bean.versicherungsnehmer;

        return subject;
    }

    private BodyPart createBodyPartFromAttachment(Attachment attachment) {
        assert(attachment != null);
        BodyPart bodyPart = new MimeBodyPart();
        try {
            DataSource dataSource = new URLDataSource(new URL(this.settingsManager.getGlobalSettings().getBaseUrl() + attachment.getUrlPath()));
            bodyPart.setDataHandler(new DataHandler(dataSource));
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
        return bodyPart;
    }

    private SMTPMailServer getSmtpMailServer() {
        return this.mailServerManager.getDefaultSMTPMailServer();
    }

    private ConfluencePopMailServer getPopMailServer() {
        return (ConfluencePopMailServer)this.mailServerManager.getDefaultPopMailServer();
    }


}