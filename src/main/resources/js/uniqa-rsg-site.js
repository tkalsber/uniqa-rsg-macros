
AJS.$(document).ready(function(){

    require(['aui/form-validation']);

    function check(item) { $(item).prop("checked", true); }
    function checkSel(sel) { $(sel).prop("checked", true); }
    function uncheckSel(sel) { $(sel).prop("checked", false); }
    function uncheck(item) { $(item).prop("checked", false); }
    function isChecked(item) { return $(item).prop("checked"); }
    function isSelChecked(sel) { return $(sel).prop("checked"); }

    function enable(item) {
        $(item).prop("checked", false);
        $(item).attr("disabled", false);
    }

    function disable(item) {
        $(item).prop("checked", false);
        $(item).attr("disabled", true);
    }

    $("#neubesichtigung").on("change", function(evt) {
        if($("#neubesichtigung").prop("checked")){
            $("#nachbesichtigung, #nachbesichtigung-schaden").prop("checked", false);
            $("#nachbesichtigung-schaden").attr("disabled", true);
        }
    });

    $("#nachbesichtigung").on("change", function(evt) {
        if($("#neubesichtigung").prop("checked")){
            $("#neubesichtigung, #nachbesichtigung-schaden").prop("checked", false);
        }
        if($("#nachbesichtigung-schaden").attr("disabled")) {
            $("#nachbesichtigung-schaden").attr("disabled", false);
        }
    });

    $("#nachbesichtigung-schaden").on("change", function(evt) {
        if($("#nachbesichtigung-schaden").prop("checked")){
            $("#nachbesichtigung").prop("checked", true);
            $("#neubesichtigung").prop("checked", false);
        } else {
            $("#nachbesichtigung").prop("checked", false);
        }
    });

    $("#betrieb-und-planen").on("change", function(evt) {
        if($("#betrieb-und-planen").prop("checked"))
            $(".b-und-b").attr("disabled", false);
        else {
            $(".b-und-b").attr("disabled", true);
            $(".b-und-b").prop("checked", false);
            disable($("#bp-pml"));
            $("#bp-beschreibung").val("");
            disable("#bp-beschreibung");
        }
    });

    $(".wert-gebaeude-info").on("click", function(evt) {
        items = $(".wert-gebaeude-info");
        isOneChecked = false;
        for(i=0; i<items.length;i++) {
            if(items[i].checked) isOneChecked = true;
        }
        if(isOneChecked){
            $("#gebaeude-zusatz").show();
            enable($("#vollmacht-bei"));
        }
        else {
            $("#gebaeude-zusatz").hide();
            disable($("#vollmacht-bei"));
        }
    });


    $(".wert-einrichtung-info").on("click", function(evt) {
        items = $(".wert-einrichtung-info");
        isOneChecked = false;
        for(i=0; i<items.length;i++) {
            if(items[i].checked) isOneChecked = true;
        }
        if(isOneChecked) $("#einrichtung-zusatz").show();
        else $("#einrichtung-zusatz").hide();
    });

    /*
     * Risikoevaluierung komplett
     */
    $("#bp-risiko-komplett").on("change", function(evt) {
        if( $("#bp-risiko-komplett").prop("checked")){
            enable($("#bp-pml"));
        } else {
            disable($("#bp-pml"));
        }
    });

    /*
     * Beschreibung NATKAT
     */
    $("#bp-risiko-natkat").on("change", function(evt) {
        if( $("#bp-risiko-natkat").prop("checked")){
            enable($("#bp-beschreibung"));
        } else {
            disable($("#bp-beschreibung"));
            $("#bp-beschreibung").val("");
        }
    });

    /*
     * Hof und Ernten
     */
    $("#hof-und-ernte").on("change", function(evt) {
        if( $("#hof-und-ernte").prop("checked")){
            enable($(".h-und-e"));
        } else {
            disable($(".h-und-e"));
            disable($("#he-flaeche"));
            disable($("#he-maschine"));
        }
    });
    $("#he-risiko").on("change", function(evt) {
        if( $("#he-risiko").prop("checked")){
            enable($("#he-flaeche"));
            enable($("#he-maschine"));
        } else {
            disable($("#he-flaeche"));
            disable($("#he-maschine"));
        }
    });

    /*
     * Industrie und Individual
     */
    $("#industrie-und-individual").on("change", function(evt) {
        if( $("#industrie-und-individual").prop("checked")){
            enable($(".i-und-i"));
        } else {
            disable($(".i-und-i"));
        }
    });

    /*
     * Immobilien und Verwaltung
     */
    $("#immobilie-und-verwaltung").on("change", function(evt) {
        if( $("#immobilie-und-verwaltung").prop("checked")){
            enable($(".i-und-v"));
        } else {
            disable($(".i-und-v"));
        }
    });

    /*
     * Sonstiges
     */
    $("#sonstiges").on("change", function(evt) {
        if( $("#sonstiges").prop("checked")){
            enable($("#sonstiges-beschreibung"));
        } else {
            $("#sonstiges-beschreibung").val("");
            disable($("#sonstiges-beschreibung"));
        }
    });


    $("#weg-f980").on("change", function(evt) {
        if (isChecked("#weg-f980")) {
            uncheck("#weg-f639");
            uncheck("#weg-f401");
        }
    });

    $("#weg-f639").on("change", function(evt) {
        if (isChecked("#weg-f639")){
            uncheck("#weg-f980");
            uncheck("#weg-f401");
        }
    });

    $("#weg-f401").on("change", function(evt) {
        if (isChecked("#weg-f401")){
            uncheck("#weg-f639");
            uncheck("#weg-f980");
        }
    });


    $("#auftraggeber-ja").on("change", function(evt) {
        if($("#auftraggeber-ja").prop("checked")){
            $("#auftraggeber-nein").prop("checked", false);
        }
    });
    $("#auftraggeber-nein").on("change", function(evt) {
        if($("#auftraggeber-nein").prop("checked")){
            $("#auftraggeber-ja").prop("checked", false);
        }
    });

    $("#betreuung-ja").on("change", function(evt) {
        if($("#betreuung-ja").prop("checked")){
            $("#betreuung-nein").prop("checked", false);
        }
    });
    $("#betreuung-nein").on("change", function(evt) {
        if($("#betreuung-nein").prop("checked")){
            $("#betreuung-ja").prop("checked", false);
        }
    });

    $("#versicherungsnehmer-ja").on("change", function(evt) {
        if($("#versicherungsnehmer-ja").prop("checked")){
            $("#versicherungsnehmer-nein").prop("checked", false);
        }
    });
    $("#versicherungsnehmer-nein").on("change", function(evt) {
        if($("#versicherungsnehmer-nein").prop("checked")){
            $("#versicherungsnehmer-ja").prop("checked", false);
        }
    });

    $("#kostenuebernahme-ja").on("change", function(evt) {
        if($("#kostenuebernahme-ja").prop("checked")){
            $("#kostenuebernahme-nein").prop("checked", false);
        }
    });
    $("#kostenuebernahme-nein").on("change", function(evt) {
        if($("#kostenuebernahme-nein").prop("checked")){
            $("#kostenuebernahme-ja").prop("checked", false);
        }
    });

    $("#planungsunterlagen-ja").on("change", function(evt) {
        if($("#planungsunterlagen-ja").prop("checked")){
            $("#planungsunterlagen-nein").prop("checked", false);
        }
    });
    $("#planungsunterlagen-nein").on("change", function(evt) {
        if($("#planungsunterlagen-nein").prop("checked")){
            $("#planungsunterlagen-ja").prop("checked", false);
        }
    });

    $("#btn-delete-address").on("click", function(evt) {
        var checkedAddresses =  $(".address:checkbox:checked");
        for(i=0; i<checkedAddresses.length; i++) {
            cbId = $(checkedAddresses[i]).attr("id");
            itemId = cbId.split("-")[1];
            $("#litem-" + itemId).remove();
        }

    });

    $("#betrieb-und-planen, #hof-und-ernte, #industrie-und-individual, #immobilie-und-verwaltung").on("change", function(evt) {
        n = $(".gebaeude:checkbox:checked").length;
        if(n > 0) $("#gebaeude-zusatz").show();
        else $("#gebaeude-zusatz").hide();
    });

    $("#betrieb-und-planen, #industrie-und-individual, #immobilie-und-verwaltung").on("change", function(evt) {
        n = $(".einrichtung:checkbox:checked").length;
        if(n > 0) $("#einrichtung-zusatz").show();
        else $("#einrichtung-zusatz").hide();
    });

    var seqid = 1000;
    $("#btn-add-address").off().on("click", function(evt) {
        var land =  $("#land option:selected").text();
        var landcode = $("#land").val();
        var strasse = $("#strasse").val();
        var hausnr = $("#hausnr").val();
        var plz = $("#plz").val();
        var ort = $("#ort").val();

        if(land == "" || landcode == "" || strasse == "" || plz == "" || hausnr == "" || ort == "") {
            alert("Fehlerhafte Adresse");
            return;
        }
        var adresse = plz + " " + ort + ", " + strasse + " " + hausnr + ", " + landcode + "-" + land;

        //<li class="list-group-item">
        //    <input type="checkbox" name="optionsRadios" id="a1" value="true" >
        //    <label>Option one is this and that&mdash;be sure to include why it's great</label>
        //</li>

        var listItem = $('<li/>', {
            'class' : 'list-group-item addressItem',
            'id' : 'litem-'+seqid
        });
        var checkBox = $('<input/>', {
            'id' : 'adr-'+seqid,
            'type' : 'checkbox',
            'class' : 'address'
        });
        var checkBoxLabel = $('<label/>', {
            'html' : '&nbsp;'+adresse
        });

        listItem.append(checkBox);
        listItem.append(checkBoxLabel);
        listItem.appendTo("#list-adressen");
        seqid++;

        // reset addressfields
        $("#land, #strasse, #hausnr, #plz, #ort").val("");

    });

    // -------------------------------------------------
    // Validate and submit form
    // -------------------------------------------------
    $("#button-submit").on("click", function(evt) {
        if( rsgValidator.validate() ) {
            var result = "";
            var addresses =  $(".address:checkbox");
            for(i=0; i<addresses.length; i++) {
                result += $(addresses[i]).next().text() + "###";
            }
            $("#adressen").val(encodeURIComponent(result));
            require(['aui/flag'], function(flag) {
                var myFlag = flag({
                    type: 'success',
                    title: 'RSG Inspection',
                    close: 'auto',
                    body: '<p>A inspection email was successfully sent.</p>'
                });
            });
            $("#rsgform").submit();
        }
        else {
            alert( rsgValidator.errorMessages() );
        }
    });

});