
var rsgValidator = (function () {

    var errorMessages = "";

//
// Helper function(s)
//

    /**
     * true if one of the specified checkboxes in checked
     * else false.
     */
    function oneOfChecked(idArr) {
        for(i=0; i<idArr.length; i++) {
            if( $(idArr[i]).prop("checked") )
                return true;
        }
        return false;
    }

    /**
     * if the 'sel' item is selected,
     * the 'field' item must not be blank or empty.
     */
    function childNotBlankIfParentSelected(sel, field, err) {
        if( $(sel).prop("checked") ) {
            if($(field).val().length <= 1)
                errorMessages += (err + "\n");

            return $(field).val().length > 1;
        }
        else
            return true;
    }

    /**
     * count number of selected items.
     * @param selArr    array holding the selectors.
     * @returns {number}
     */
    function countSelectionOf(selArr) {
        selected = 0;
        if(selArr == null) {
            AJS.log("selArr array holding the selectors can not be null, returning 0.")
            return selected;
        }
        // check all selectors
        for(i=0;i<selArr.length;i++) {
            if( $(selArr[i]).prop("checked") ) {
                selected++;
            }
        }

        return selected;
    }

//
// Validation rules as specified in https://confluence.uniqa.at/pages/viewpage.action?pageId=40587912
//

    /**
     * R1: Neubesichtigung or NAchbesichtigung selected
     * @returns {*|jQuery}
     */
    function neubesichtigungOderNachbesichtigung() {
        if(! oneOfChecked(["#neubesichtigung", "#nachbesichtigung"]) ) {
            errorMessages += "- Neubesichtigung oder Nachbesichtigung auswaehlen\n";
            return false;
        }
        else return true;
    }


    /**
     * R2: check if min one of
     *      "Betrieb & Planen",
     *      "Hof & Ernte",
     *      "Industrie & Individual",
     *      "Immobilie & Verwaltung" or
     *      "Sonstiges" is selected.
     *
     * @returns {boolean}
     */
    function minOneOf(errMsg) {
        selArr = [ "#betrieb-und-planen", "#hof-und-ernte", "#industrie-und-individual", "#immobilie-und-verwaltung", "#sonstiges" ];
        if(countSelectionOf(selArr) > 0) return true;
        else {
            errorMessages += "- min 1 Auswahl von 'Betrieb & Planen', 'Hof & Ernte', 'Industrie & Individual', 'Immobilie & Verwaltung' oder 'Sonstiges' notwendig\n";
            return false;
        }
    }

    /**
     * R4: if 'Wertermittlung Gebäude' is selected, one of the childoptions must be selected.
     * @returns {boolean}
     */
    function zusatzGebaeudeSchaetzen() {
        if($(".gebaeude:checkbox:checked").length > 0) {
            if(! oneOfChecked(["#weg-f980", "#weg-f639", "#weg-f401"]) ) {
                errorMessages += "- Zusatzdaten fuer 'Gebaeude' fehlt\n";
                return false;
            }
            else return true;
        }
        else return true;
    }

    /**
     * R5: if 'Wertermittlung Einrichtung' is selected, a description is necessary.
     * @returns {boolean}
     */
    function zusatzEinrichtungBeschreibung() {
        if($(".einrichtung:checkbox:checked").length > 0) {
            if($("#einrichtung-beschreibung").val().length <= 1) {
                errorMessages += "- Beschreibung fuer 'Wertermittlung Einrichtung' fehlt\n";
                return false;
            }
            else return true;
        }
        else return true;
    }

    /**
     * R6: Versicherungsnehmer name is required and either email or phone.
     *  name & tel muss, polizze & email opt
     * @returns {boolean}
     */
    function versicherungsnehmer() {
        if( $('#versicherungsnehmer').val().length <= 1 ) {    // ||  $('#versicherungsnehmer-telefon').val().length <= 1) {
            errorMessages += "- Name des Versicherungsnehmers notwendig\n";
            return false;
        }
        else {
            /*
            if( $('#versicherungsnehmer-polizze').val().length <= 1 )
                return true;

            if( $('#versicherungsnehmer-telefon').val().length <= 1 && $('#versicherungsnehmer-email').val().length <= 1 ) {
                errorMessages += "- Telefon oder Email des Versicherungsnehmers fehlt\n";
                return false;
            }
            */
            return true;
        }

        /*
         if( !( $('#versicherungsnehmer').val().length > 1 && $('#versicherungsnehmer-polizze').val().length > 1 )
         || ( $('#versicherungsnehmer').val().length > 1 && ( $('#versicherungsnehmer-telefon').val().length > 1 || $('#betreuung-email').val().length > 1 ) ) )
         errorMessages += "- Versicherungsnehmerdaten fehlerhaft\n";

         return ( $('#versicherungsnehmer').val().length > 1 && $('#versicherungsnehmer-polizze').val().length > 1 )
         || ( $('#versicherungsnehmer').val().length > 1 && ( $('#versicherungsnehmer-telefon').val().length > 1 || $('#betreuung-email').val().length > 1 ) );
         */
    }

    /**
     * R7: Auftraggeber
     * @returns {boolean}
     */
    function auftraggeber() {
        if( $('#auftraggeber-name').val().length > 1 ) {
            if( $('#auftraggeber-telefon').val().length > 1 && $('#auftraggeber-email').val().length > 1 )
                return true;
            else {
                errorMessages += "- Telefon oder Email des Auftraggebers fehlt\n";
                return false;
            }
        }
        else {
            errorMessages += "- Name des Auftraggebers fehlt\n";
            return false;
        }

    }

    /**
     * R8: Betreuuung / Makler
     *
     * @see <a href="https://jira.uniqa.at/browse/PR15IA2-4966">PR15IA2-4966</a>
     *
     * @returns {boolean}
     */
    function betreuungMakler() {
        return true;
        /*
        if ( $('#vertriebspartner').val().length > 1 ) {
            if( $('#betreuung-telefonnummer').val().length <= 1 && $('#betreuung-email').val().length <= 1 )
                errorMessages += "- Betreuer oder Maklerdaten fehlerhaft\n";

            return $('#betreuung-telefonnummer').val().length > 1 || $('#betreuung-email').val().length > 1;
        }
        else
            return true;
         */
    }





    /**
     * if checkbox "Betrieb & Planen" is selected
     *    minimum one selection of
     *        "Wertermittlung Gebäude", "Wertermittlung Einrichtung", "Risikoevaluierung komplett", "Risiko Feuer", "Risikoevaluierung ED" or "Risikoevaluierung NATKAT"
     * is required.
     */
    function betriebUndPlanenChildSelected() {
        if ( $("#betrieb-und-planen").prop("checked") ) {

            if( $(".b-und-b:checkbox:checked").length <= 0 )
                errorMessages += "- Betrieb und Planen Angabe fehlt\n";

            return $(".b-und-b:checkbox:checked").length > 0;
        }
        else
            return true;
    }

    /**
     * if checkbox "Hof & Ernten" is selected
     *      a selection of "Wertermittlung Gebäude" or "Risikoevaluierung (Landwirtschaft)"
     * is required.
     */
    function hofUndErntenChildSelected() {
        if( $("#hof-und-ernte").prop("checked") ) {

            if( $(".h-und-e:checkbox:checked").length <= 0 )
                errorMessages += "- Hof und Ernte Angabe fehlt\n";

            return $(".h-und-e:checkbox:checked").length > 0;
        }
        else
            return true;
    }

    /**
     * if "Industrie & Individual" is selected
     *      minimum one selection of "Wertermittlung Gebäude", "Wertermittlung Einrichtung" or "Risikoevaluierung"
     * is required.
     */
    function industrieUndIndividualChildSelected() {
        if( $("#industrie-und-individual").prop("checked") ) {

            if( $(".i-und-i:checkbox:checked").length <= 0)
                errorMessages += "Industrie und Individual Angabe fehlt\n";

            return $(".i-und-i:checkbox:checked").length > 0;
        }
        else
            return true;
    }

    /**
     * if "Immobilie & Verwaltung" is selected
     *      minimum one selection of "Wertermittlung Gebäude", "Wertermittlung Einrichtung" or "Risikoevaluierung"
     * is required.
     */
    function immobilieUndVerwaltungChildSelected() {
        if( $("#immobilie-und-verwaltung").prop("checked") ) {
            if( $(".i-und-v:checkbox:checked").length <= 0) {
                errorMessages += "Immobilie und Verwaltung Angabe fehlt\n";
                return false;
            }
            else return true;
        }
        else return true;
    }

    function risikoanschriften() {
        if($(".address").length <= 0 ) {
            errorMessages += "- Mindestens 1 Risikoanschrift notwendig\n";
            return false;
        }
        return true;
    }

    function isPhoneNumber(field, errmsg) {
        var re = /[0-9/(/) /+/-//]+$/;
        var s = $(field).val();
        if(s.length >= 1) {
            var valid = re.test(s);
            if (valid) return true;
            else {
                errorMessages += (errmsg + "\n");
                return false;
            }
        }
        else {
            console.log("empty field, phonecheck = true");
            return true;
        }
    }

    /**
     * validate all rules.
     */
    function validateForm() {
        errorMessages = "";
        return neubesichtigungOderNachbesichtigung()
            && minOneOf()
            && hofUndErntenChildSelected()
            && betriebUndPlanenChildSelected()
            && industrieUndIndividualChildSelected()
            && immobilieUndVerwaltungChildSelected()
            && childNotBlankIfParentSelected('#sonstiges', '#sonstiges-beschreibung', "- Beschreibung von 'Sonstiges' ist leer\n")
            && childNotBlankIfParentSelected('#bp-risiko-natkat', '#bp-beschreibung', '- NATKAT Beschreibung ist leer')
            && zusatzGebaeudeSchaetzen()
            && zusatzEinrichtungBeschreibung()
            && auftraggeber()
            && betreuungMakler()
            && versicherungsnehmer()
            && risikoanschriften()
            && isPhoneNumber("#auftraggeber-telefon", "- Auftraggeber Telefonnummer ungueltig\n")
            && isPhoneNumber("#betreuung-telefonnummer", "- Betreuer/Makler Telefonnummer ungueltig\n")
            && isPhoneNumber("#versicherungsnehmer-telefon", "- Versicherungsnehmer Telefonnummer ungueltig\n");
    }

    function getErrorMessages() {
        return errorMessages;
    }

    // expose public interface
    return {
        errorMessages : getErrorMessages,
        validate: validateForm
    };

}());

